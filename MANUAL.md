
# Options

    --infile FILESPEC
        input file spec

    --outdir PATH
        output directory

    --outfile FILENAME
        output file name (ignored when multiple files)

    --outext EXT
        output extensions (default for .xlsx in => .csv, for .csv in => .xlsx) 

    --overwrite
        overwrite existing files

    --encoding ENCNAME
        set encoding

    --dateformat-csv
    --dateformat-excel
        set date formats
    
    --sheet NAME or NUMINDEX

    --colsep
    --rowsep

    --headerlabels
        comma-separated list of header labels
    --headerlines
        number of header lines in .csv or .xlsx

    --rows
    --columns
    --range

    --fontname
    --fontsize

    --silent

    --autoformula

    --append
